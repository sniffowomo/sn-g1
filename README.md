
m | m 
|:--:|:--:|
<img src="https://img-cf.xvideos-cdn.com/videos/thumbs169ll/49/25/b3/4925b3594525e506a9f58f43a12eeeb7-1/4925b3594525e506a9f58f43a12eeeb7.14.jpg"> | <img src="https://i.imgflip.com/1lxmac.gif">


----

<h1 align="center"><code> SN-G1 </code></h1>
<h2 align="center"><i> Tezta dat shizzzaa </i></h2>

----

1. [😯](#)
2. [Coms](#coms)
   1. [Cumulative](#cumulative)
   2. [`NJ13 Install`](#nj13-install)
   3. [Tailwind Pretier](#tailwind-pretier)
   4. [Tailwind Animated](#tailwind-animated)
   5. [Daisy UI](#daisy-ui)
      1. [Config important](#config-important)
      2. [Theme Per Element](#theme-per-element)
3. [`VHS`](#vhs)
4. [Dira](#dira)

----

# 😯 

> This will just have all tests being done to see which one branchista


# Coms 

## Cumulative 

```sh 
pnpm dlx create-next-app@latest
```
then 

```sh 
pnpm i -D prettier prettier-plugin-tailwindcss
pnpm i tailwindcss-animated
pnpm i -D daisyui@latest
```

## [`NJ13 Install`](https://nextjs.org/docs) 


> https://nextjs.org/docs 

```sh 
pnpm dlx create-next-app@latest
```

## [Tailwind Pretier](https://tailwindcss.com/blog/automatic-class-sorting-with-prettier) 

> https://tailwindcss.com/blog/automatic-class-sorting-with-prettier

```sh 
pnpm i -D prettier prettier-plugin-tailwindcss
```

## Tailwind Animated 

```sh 
pnpm i tailwindcss-animated
```
Now add to 
`tailwind.config.js`

```sh 
// tailwind.config.js
module.exports = {
  // ...
  plugins: [
    require('tailwindcss-animated')
  ],
}
```

## Daisy UI 

> https://daisyui.com/ 

```sh 
pnpm i -D daisyui@latest
```

### Config important 

`tailwind.config.js`

```json
module.exports = {
  //...

  // add daisyUI plugin
  plugins: [require("daisyui")],

  // daisyUI config (optional - here are the default values)
  daisyui: {
    themes: true, // true: all themes | false: only light + dark | array: specific themes like this ["light", "dark", "cupcake"]
    darkTheme: "dark", // name of one of the included themes for dark mode
    base: false, // applies background color and foreground color for root element by default
    styled: true, // include daisyUI colors and design decisions for all components
    utils: true, // adds responsive and modifier utility classes
    rtl: false, // rotate style direction from left-to-right to right-to-left. You also need to add dir="rtl" to your html tag and install `tailwindcss-flip` plugin for Tailwind CSS.
    prefix: "", // prefix for daisyUI classnames (components, modifiers and responsive class names. Not colors)
    logs: true, // Shows info about daisyUI version and used config in the console when building your CSS
  },

  //...
}
```

### Theme Per Element 

```html 
<html data-theme="cupcake"> pussy </html>
```

# [`VHS`](https://github.com/charmbracelet/vhs)

> https://github.com/charmbracelet/vhs

Install 
```sh 
brew install vhs
```

# Dira 

> List of dira fuokfuking 

👍 | 💯 
|:--:|:--:|